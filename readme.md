# 答题程序题目生成工具

配合答题小程序使用的题目生成工具。
答题小程序地址： https://gitee.com/planb16/online-exam-weapp

## 开发&部署方式

### 初始化
```bash
npm install
```
### 调试
本项目使用 parcel 打包，运行下面命令将在 `http://localhost:1234/` 运行一个同步刷新的本地调试服务。

```bash
npm run start
```

### 打包

```bash
npm run build
```
其中的参数`--dist-dir deploy` 指定打包文件`deploy`目录；

参数 `--public-url /exam-tool/` 指定部署后的资源相对路径。

### 部署到 Netlify
作为演示，已经将 `--dist-dir` 中的代码部署了一份在 https://playground.jslab.pro/exam-tool/

### 演示
题目和选项（不包含题号）相同的题目只会录入一次，同时，云数据库需要建一个字段 `md5` 的唯一索引，导入时选择 `Upsert：如果记录存在则更新，否则插入新记录`，就会自动忽略已添加的题目。

目前支持的格式有单选题和多选题，其他题型会忽略：
```
1、我国的火警报警电话是？（C）
A、110
B、120
C、119
D、911
解析：我国的火警报警电话是119。

2、我国的火警报警电话是？（C）
A、110
B、120
C、119
D、911
解析：我国的火警报警电话是119。

2、四大文明古国中的，四大文明是下列选项中的哪四个？（ABCE）[多选题]
A、埃及文明
B、印度文明
C、中国文明
D、希腊文明
E、美索不达米亚文明
解析：传统认为，四大文明分别是中国、埃及、印度和两河流域的美索不达米亚。

4、《史记》的作者是？[填空题]
答案：司马迁

10.When the temperature is below zero, water will _______.(A)[单选题]
A.freeze
B.virtue
C.sink
D.seek
```
