import { prettyPrintJson } from "pretty-print-json";
import copy from "copy-to-clipboard";
import gotpl from "gotpl";
import md5 from "md5";

const regexQuestionSplit = /\n\s*\n/gm;
const regexQuestionRowSplit = /\n/gm;
const regexTitle1 =
  /(?<index>\d+)[.、][\s]*(?<title>.*?)[(（](?<answer>[A-Z]+)[)）]$/;
const regexTitle2 =
  /(?<index>\d+)[.、][\s]*(?<title>.*?)[(（](?<answer>[A-Z]+)[)）][\[【](?<type>\W+)[\]】]$/;
const regexOption = /(?<value>[a-zA-Z])\s*?[.、]\s*?(?<text>.*)$/;
const regexDesc = /解析[:：]\s*?(?<desc>.*)$/;

let resultObj = {
  id: 0,
  name: "",
  questions: [],
};
let resultJson = "";

const tpl = `
<div class="question">
  <div class="row">
    问卷ID：<%= id %>
  </div>
  <div class="row">
    问卷名称：<%= name %>
  </div>
  <% for(var i=0, l=questions.length; i<l; ++i){ %>
    <% var item = questions[i]; %>
    <div class="question-wrap">
      <div class="question-title"><%= i+1 %>. <%= item.title %>【<%=item.typeText %>】</div>
      <% if(item.type === 'radio'){ %>
        <div class="radio">
          <% for(var j=0, k=item.options.length; j<k; ++j){ %>
            <% var isCorrect = item.answer.indexOf(item.options[j].value) > -1 %>
            <label class="label">
              <input 
                type="radio" 
                name="q_<%= i+1 %>" 
                value="<%= item.options[j].value %>" 
                <% if(isCorrect){ %>
                  checked
                <% } %>
              />
              <%= item.options[j].value %>.
              <%= item.options[j].text %>
              <% if(item.answer.indexOf(item.options[j].value) > -1){ %>
                <span class="answer">(正确答案)</span>
              <% } %>
            </label>
          <% } %>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% }else if(item.type === 'checkbox'){ %>
        <div class="checkbox">
          <% for(var j=0, k=item.options.length; j<k; ++j){ %>
            <% var isCorrect = item.answer.indexOf(item.options[j].value) > -1 %>
            <label class="label">
              <input 
                type="checkbox"  
                name="q_<%= i+1 %>" 
                value="<%= item.options[j].value %>" 
                <% if(isCorrect){ %>
                  checked
                <% } %>
              />
              <%= item.options[j].value %>.
              <%= item.options[j].text %>
              <% if(isCorrect){ %>
                <span class="answer">(正确答案)</span>
              <% } %>
            </label>
          <% } %>
        </div>
        <% if(item.desc){ %>
          <div class="question-desc">
            解析：<%= item.desc %>
          </div>
        <% } %>
      <% } %>
    </div>
  <% } %>
</div>
`;

const type_map = {
  填空题: "input",
  单选题: "radio",
  多选题: "checkbox",
};
function getQuestionType(questionType) {
  return type_map[questionType] || "类型错误";
}
function formatQuestion(title, typeText, answer, desc, options = null) {
  // console.log(index, title, questionType);
  const questionType = getQuestionType(typeText);
  const answerArr = [...answer];

  let question = {
    md5: md5(title + options.join("")),
    title,
    type: questionType,
    typeText,
    answer: answerArr,
    desc,
    options: options.reduce((acc, cur) => {
      if (regexOption.test(cur)) {
        const { value, text } = cur.match(regexOption).groups;
        return [
          ...acc,
          {
            value,
            text,
          },
        ];
      }
    }, []),
  };

  return question;
}

// 重复题目不添加
function addQuestion(question) {
  if (resultObj.questions.find((item) => item.md5 === question.md5)) {
    return;
  }
  resultObj.questions.push(question);
}

document.getElementById("btn").addEventListener("click", function () {
  resultObj.id = +document.getElementById("qid").value.trim();
  resultObj.name = document.getElementById("qname").value.trim();

  // 用户输入的字符串
  let text = document.getElementById("text").value;
  // console.log(text);

  // 按空行分割题目
  let questionArr = text.split(regexQuestionSplit);
  // console.log(questionArr);

  // 开始对每个题目进行处理
  questionArr.forEach((q) => {
    // 分割每一行
    let rows = q.split(regexQuestionRowSplit);
    // console.log(JSON.stringify(rows));

    // 去掉空行
    rows = rows.filter((item) => item.trim() !== "");
    // console.log(JSON.stringify(rows));

    // 全部去掉前后空格
    rows = rows.reduce((acc, cur) => {
      return [...acc, cur.trim()];
    }, []);
    // console.log(JSON.stringify(rows));

    // console.log(questions);

    // 多行，可能是单选或多选
    if (rows.length > 1) {
      // 第一行是标题，其他行是选项
      let [titleRow, ...options] = rows;
      // console.log("titleRow", titleRow);
      // console.log("options", options);

      // 最后一行可能是答案解析，测试看看
      const lastOption = options.slice(-1)[0];
      // console.log("lastOption", lastOption);
      let desc = null;
      if (regexDesc.test(lastOption)) {
        // 最后一个是解析
        let matchsDesc = lastOption.match(regexDesc);
        desc = matchsDesc.groups.desc;
        // 最后一个是解析，从options中去掉。
        options.pop();
      }

      // 先验证带题目类型的格式
      if (regexTitle2.test(titleRow)) {
        let matches = titleRow.match(regexTitle2);
        //  console.log(matches)
        let { index, title, answer, type } = matches.groups;
        addQuestion(formatQuestion(title, type, answer, desc, options));
      } else if (regexTitle1.test(titleRow)) {
        // 没有设置类型的，当成单选题
        let matches = titleRow.match(regexTitle1);
        let { index, title, answer } = matches.groups;
        // console.log(index, title, answer, options);
        addQuestion(formatQuestion(title, "单选题", answer, desc, options));
      }
      // console.log(questions);
    }
  });

  resultJson = JSON.stringify(resultObj);
  // console.log(resultObj);
  document.getElementById("json-preview").innerHTML =
    prettyPrintJson.toHtml(resultObj);

  document.getElementById("html-preview").innerHTML = gotpl.render(
    tpl,
    resultObj
  );
});

document.getElementById("copy").onclick = () => {
  copy(resultJson);
  alert("已复制到剪贴板");
};

document.getElementById("download").onclick = () => {
  let strResult = resultObj.questions.reduce((acc, cur) => {
    return `${acc}${JSON.stringify(cur)}\n`;
  }, "");

  const filename = "questions_" + new Date().toLocaleDateString() + ".json";

  // Start file download.
  var blob = new Blob([strResult], { type: "text/plain" });
  var url = window.URL.createObjectURL(blob);
  var a = document.createElement("a");
  a.href = url;
  a.download = filename;
  a.click();
  window.URL.revokeObjectURL(url);
};
